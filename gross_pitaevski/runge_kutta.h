#ifndef RUNGE_KUTTA_GPE_SOLVE_H__
#define RUNGE_KUTTA_GPE_SOLVE_H__
#include "gpe_solve.h"

namespace mephi
{

class RungeKuttaGPESolver: public GPESolver
{
public:
    RungeKuttaGPESolver(const GPEConfig& cfg)
        : GPESolver(cfg)
    {}
    virtual ~RungeKuttaGPESolver() {}
    RungeKuttaGPESolver(const RungeKuttaGPESolver&) = delete;
    RungeKuttaGPESolver& operator=(const RungeKuttaGPESolver&) = delete;

    // compute values after given number of time intervals (tstep)
    // 0 means cfg_.tpoints
    virtual void ComputeValues(size_t intervals = 0) override;
private:
    Complex f(Float x, const Complex& psi0, const Complex& psi1, const Complex& psi2);
};

}
#endif // RUNGE_KUTTA_GPE_SOLVE_H__
