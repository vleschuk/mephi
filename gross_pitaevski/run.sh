#!/bin/bash
./gpe_solve \
        --xpoints 80 \
        --tstep 0.0001 \
        --tpoints 100000 \
        --u0 -30 \
        --lb 0.0 --rb 0.0 \
        --plot --nplots 3 --animation 1000 #--png "test.png"
