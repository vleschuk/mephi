#ifndef GPE_SOLVE_H__
#define GPE_SOLVE_H__
#include <vector>
#include <complex>

namespace mephi
{

struct GPEConfig
{
    GPEConfig(double leftb = 0.0, double rightb = 0.0, double am = 1.0,
              size_t xpoints = 100, size_t tpoints = 100, double tstep = 0.001, double u0 = -0.1,
              bool plot = false, size_t nplots = 5, size_t animation = 0);
    double left_bound; // f(0,0)
    double right_bound; // f(1,0)
    double ampl; // start function amplitude
    size_t xpoints;
    size_t tpoints;
    double xstep;
    double tstep;
    double u0;
    bool do_plot;
    size_t nplots;
    size_t animation; // tpoints for each step, 0 - no animation
    char pngfile[16];
    char method[16]; // solving method
};

class GPESolver
{
public:
    typedef double Float;
    typedef std::complex<Float> Complex;
    typedef std::vector<Float> FloatValues;
    typedef std::vector<Complex> ComplexValues;
public:
    static const Complex ii;
    static const Float pi;
public:
    GPESolver(const GPEConfig& cfg);
    virtual ~GPESolver() {}
    GPESolver(const GPESolver&) = delete;
    GPESolver& operator=(const GPESolver&) = delete;

    void ComputeInitialValues();
    const ComplexValues& GetInitialValues() const { return initial_; }

    const FloatValues& GetCoords() const { return coords_; }

    // compute values after given number of time intervals (tstep)
    // 0 means cfg_.tpoints
    virtual void ComputeValues(size_t intervals = 0) = 0;
    const ComplexValues& GetValues() const { return current_; }

protected:
    const GPEConfig& cfg_;
    
    FloatValues coords_; // coordinates vector: used to plot coords (0.0, 1.0) instead of vector indexes
    ComplexValues initial_;
    ComplexValues current_;
    size_t curr_interval_; // number of current time interval
};
}
#endif // GPE_SOLVE_H__
