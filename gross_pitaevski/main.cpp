#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <getopt.h>
#include <memory>
#include <cstring>
#include <string>
#include <tuple>
#include "runge_kutta.h"
#include "gnuplot-iostream.h"

namespace
{
std::string abs_output;
mephi::GPEConfig cfg;
size_t period_len;
typedef std::vector<mephi::GPESolver::ComplexValues> ComplexNumbersMatrix;
typedef std::vector<std::vector<mephi::GPESolver::Float>> FloatNumbersMatrix; // helper - matrix of underlying float type
typedef std::unique_ptr<mephi::GPESolver> SolverPtr;

struct option long_options[] = {
        {"lb", required_argument, 0, 'l'},
        {"rb", required_argument, 0, 'r'},
        {"ampl", required_argument, 0, 'a'},
        {"xpoints", required_argument, 0, 'x'},
        {"tpoints", required_argument, 0, 'T'},
        {"tstep", required_argument, 0, 't'},
        {"u0", required_argument, 0, 'u'},
        {"png", required_argument, 0, 'o'},
        {"method", required_argument, 0, 'm'},
        {"abs-dump", required_argument, 0, 'd'},
        {"plot", no_argument, 0, 'p'},
        {"nplots", required_argument, 0, 'n'},
        {"animation", required_argument, 0, 'A'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
};

void ShowUsage()
{
    const auto *opt = long_options;
    std::cerr << "\nUsage:\n";
    for(; opt && opt->name; ++opt)
    {
        std::cerr << "\t--" << opt->name;
        if(required_argument == opt->has_arg)
        {
            std::cerr << " <arg>";
        }
        else if(optional_argument == opt->has_arg)
        {
            std::cerr << " [arg]";
        }
        std::cerr << std::endl;
    }
}

bool ParseCmd(int argc, char **argv, mephi::GPEConfig& cfg)
{
    int c;
    bool ok = true;
    while(true)
    {
        int option_index = 0;
        c = getopt_long(argc, argv, "l:r:c:a:x:T:t:u:o:m:d:pn:A:h", long_options, &option_index);

        if(-1 == c)
        {
            break;
        }

        switch(c)
        {
        case 'l':
            cfg.left_bound = atof(optarg);
            break;
        case 'r':
            cfg.right_bound = atof(optarg);
            break;
        case 'a':
            cfg.ampl = atof(optarg);
            break;
        case 'x':
            cfg.xpoints = atol(optarg);
            cfg.xstep = 1.0 / cfg.xpoints;
            break;
        case 'T':
            cfg.tpoints = atol(optarg);
            break;
        case 't':
            cfg.tstep = atof(optarg);
            break;
        case 'u':
            cfg.u0 = atof(optarg);
            break;
        case 'o':
            strncpy(cfg.pngfile, optarg, sizeof(cfg.pngfile));
            break;
        case 'm':
            strncpy(cfg.method, optarg, sizeof(cfg.method));
            break;
        case 'd':
            abs_output = optarg;
            break;
        case 'p':
            cfg.do_plot = true;
            break;
        case 'n':
            cfg.nplots = atoi(optarg);
            break;
        case 'A':
            cfg.animation = atoi(optarg);
            break;
        case 'h':
        case '?':
        default:
            ok = false;
            break;
        }
    }

    if(!ok)
    {
        ShowUsage();
    }
    period_len = cfg.tpoints / cfg.nplots;
    return ok;
}

void DumpConfig(const mephi::GPEConfig& cfg, std::ostream& stream = std::cout)
{
    stream << "# Config:\n"
        << "#\tleft_bound: " << cfg.left_bound << "\n"
        << "#\tright_bound: " << cfg.right_bound << "\n"
        << "#\tampl: " << cfg.ampl << "\n"
        << "#\txpoints: " << cfg.xpoints << "\n"
        << "#\ttpoints: " << cfg.tpoints << "\n"
        << "#\txstep: " << cfg.xstep << "\n"
        << "#\ttstep: " << cfg.tstep << "\n"
        << "#\tu0: " << cfg.u0 << "\n"
        << "#\tdo_plot: " << cfg.do_plot << "\n"
        << "#\tnplots: " << cfg.nplots << "\n"
        << "#\tpng: " << cfg.pngfile << "\n"
        << "#\tanimation: " << cfg.animation << "\n"
        << "#\tmethod: " << cfg.method << std::endl;

}

void DumpValues(const mephi::GPESolver::ComplexValues& values, size_t nperiod)
{
    for(size_t i = 0; i < values.size(); ++i)
    {
        std::cout << i << "\t" << std::fixed << std::setprecision(10) << values[i] << std::endl;
    }
}

void GetRealImagParts(const mephi::GPESolver::ComplexValues c,
                      mephi::GPESolver::FloatValues& real,
                      mephi::GPESolver::FloatValues& imag)
{
    real.clear();
    imag.clear();
    for(const auto& v : c)
    {
        real.push_back(v.real());
        imag.push_back(v.imag());
    }
}

void PreparePlot(Gnuplot& gp)
{
    gp << "set grid" << std::endl;
    gp << "show grid" << std::endl;
    gp << "unset autoscale y" << std::endl;
    gp << "set yrange [-2:2]" << std::endl;
    gp << "set xlabel 'X'" << std::endl;
    gp << "set ylabel 'Wave function'" << std::endl;
}

void DumpComplexNumbersMatrix(const mephi::GPESolver::FloatValues& coords, const ComplexNumbersMatrix& mv)
{
    if(!cfg.do_plot)
    {
        return;
    }
    FloatNumbersMatrix real(mv.size());
    real.reserve(mv.size());
    FloatNumbersMatrix imag(mv.size());
    imag.reserve(mv.size());
    std::ostringstream ss;
    bool comma = false;
    ss << "plot";
    for(size_t i = 0; i < mv.size(); ++i)
    {
        real[i].reserve(mv[i].size());
        imag[i].reserve(mv[i].size());
        std::for_each(mv[i].begin(), mv[i].end(), [&real, &imag, i](const mephi::GPESolver::Complex& v) {
            real[i].push_back(v.real());
            imag[i].push_back(v.imag());
        });
        if(comma)
            ss << ",";
        if(!comma) comma = true;
        const auto n = i*period_len;
        ss << " '-' with lines title 'real values, " << n << " time points'"
           << ", '-' with points title 'imag values, " << n << " time points'";
    }
    std::cerr << "DEBUG: " << ss.str() << std::endl;
    Gnuplot gp;
    PreparePlot(gp);

    if(cfg.pngfile && cfg.pngfile[0])
    {
        gp << "set terminal png" << std::endl;
        gp << "set output '" << cfg.pngfile << "'" << std::endl;
    }

    gp << ss.str() << std::endl;
    for(size_t i = 0; i < mv.size(); ++i)
    {
        gp.send1d(std::tie(coords, real[i]));
        gp.send1d(std::tie(coords, imag[i]));
    }
}

void SimpleComputation(SolverPtr&& slv)
{
    ComplexNumbersMatrix multiple_values;
    multiple_values.push_back(slv->GetInitialValues());

    for(size_t i = 1; i <= cfg.nplots; ++i)
    {
        const auto n = i*period_len;
        slv->ComputeValues(n);
        multiple_values.push_back(slv->GetValues());
        std::cout << "Values after " << n << " intervals:" << std::endl;
        DumpValues(slv->GetValues(), n);
    }
    DumpComplexNumbersMatrix(slv->GetCoords(), multiple_values);
}

void sleep_ms(size_t msec)
{
    if(0 != usleep(msec * 1000))
    {
        std::cerr << "WARN: usleep failed: " << strerror(errno) << std::endl;
    }
}

void AnimatedComputation(SolverPtr&& slv)
{
    Gnuplot gp;

    mephi::GPESolver::FloatValues real;
    real.reserve(slv->GetValues().size());
    mephi::GPESolver::FloatValues imag;
    imag.reserve(slv->GetValues().size());

    size_t n = 0;
    do
    {
        std::cout << "Values after " << n << " intervals:" << std::endl;
        GetRealImagParts(slv->GetValues(), real, imag);
        gp << "set terminal x11 noraise title '" << n << " time points'" << std::endl;
        PreparePlot(gp);
        gp << "plot '-' with lines title 'real values', '-' with points title 'imag values'" << std::endl;
        gp.send1d(std::tie(slv->GetCoords(),real));
        gp.send1d(std::tie(slv->GetCoords(),imag));
        gp.flush();

        n += cfg.animation;
        slv->ComputeValues(n);
        DumpValues(slv->GetValues(), n);
        sleep_ms(100);
    } while(true);
}

} // anonymous namespace

int main(int argc, char **argv)
{
    try
    {
        if(!ParseCmd(argc, argv, cfg))
        {
            return 1;
        }
        DumpConfig(cfg);
        SolverPtr slv;

        // Currently only 1 method is supported, however other can be added later
        if(nullptr == cfg.method || '\0' == cfg.method[0] || 0 == strcmp(cfg.method, "runge-kutta"))
        {
            slv.reset(new mephi::RungeKuttaGPESolver(cfg));
        }
        else
        {
            std::cerr << "Unknown method: " << cfg.method << std::endl;
            return 1;
        }

        slv->ComputeInitialValues();

        if(0 != cfg.animation)
        {
            AnimatedComputation(std::move(slv));
        }
        else
        {
            SimpleComputation(std::move(slv));
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }
    catch(...)
    {
        std::cerr << "Unknown error!" << std::endl;
        return 1;
    }
}
