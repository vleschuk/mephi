/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdexcept>
#include <cmath>
#include <limits>
#include "runge_kutta.h"

namespace mephi
{

void RungeKuttaGPESolver::ComputeValues(size_t intervals)
{
    if(0 == intervals)
    {
        intervals = cfg_.tpoints;
    }

    const Float dt = cfg_.tstep;
    const Float lb = cfg_.left_bound;
    const Float rb = cfg_.right_bound;

    auto PrepareArray = [=](ComplexValues& v) {
        v.resize(cfg_.xpoints);
        v.reserve(cfg_.xpoints);
        v[0] = lb;
        v[cfg_.xpoints - 1] = rb;
    };

    ComplexValues k1, k2, k3, k4;
    PrepareArray(k1);
    PrepareArray(k2);
    PrepareArray(k3);
    PrepareArray(k4);

    ComplexValues tmp(cfg_.xpoints);
    tmp.reserve(cfg_.xpoints);
    for(; curr_interval_ < intervals; ++curr_interval_)
    {
        for(size_t i = 1; i < cfg_.xpoints - 1; ++i)
        {
            const auto x = cfg_.xstep * i;
            k1[i] = f(x, current_[i-1], current_[i], current_[i+1]);
        }

        for(size_t i = 1; i < cfg_.xpoints - 1; ++i)
        {
            const auto x = cfg_.xstep * i;
            k2[i] = f(x, current_[i-1] + k1[i-1]*(dt/2),
                         current_[i] + k1[i]*(dt/2),
                         current_[i+1] + k1[i+1]*(dt/2));
        }

        for(size_t i = 1; i < cfg_.xpoints - 1; ++i)
        {
            const auto x = cfg_.xstep * i;
            k3[i] = f(x, current_[i-1] + k2[i-1]*(dt/2),
                         current_[i] + k2[i]*(dt/2),
                         current_[i+1] + k2[i+1]*(dt/2));
        }

        for(size_t i = 1; i < cfg_.xpoints - 1; ++i)
        {
            const auto x = cfg_.xstep * i;
            k4[i] = f(x, current_[i-1] + k3[i-1]*dt,
                         current_[i] + k3[i]*dt,
                         current_[i+1] + k3[i+1]*dt);
        }

        tmp[0] = lb;
        for(size_t i = 1; i < cfg_.xpoints - 1; ++i)
        {
            tmp[i] = current_[i] +
                    (k1[i] + k2[i] + k2[i] + k3[i] + k3[i] + k4[i])*(dt/6);
        }
        tmp[cfg_.xpoints - 1] = rb;
        current_.swap(tmp);
    }
}

GPESolver::Complex RungeKuttaGPESolver::f(Float x, const Complex& psi0,
                            const Complex& psi1,
                            const Complex& psi2)
{
    const auto abs_psi1 = std::abs(psi1);
    return  -ii * (
                    -(psi2 - psi1 - psi1 + psi0) / (Float)cfg_.xstep / (Float)cfg_.xstep
                    + (Float)cfg_.u0 * abs_psi1 * abs_psi1 * psi1
                  );
}

} // namespace mephi
