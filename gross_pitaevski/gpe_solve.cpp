/* vim: set tabstop=4 shiftwidth=4 expandtab : */
#include <stdexcept>
#include <cmath>
#include <limits>
#include <cstring>
#include "gpe_solve.h"

namespace mephi
{

GPEConfig::GPEConfig(double leftb, double rightb, double am,
          size_t xp, size_t tp, double ts, double u, bool plot, size_t np, size_t an)
    : left_bound(leftb),
      right_bound(rightb),
      ampl(am),
      xpoints(xp),
      tpoints(tp),
      xstep(1.0/(xpoints - 1)),
      tstep(ts),
      u0(u),
      do_plot(plot),
      nplots(np),
      animation(an)
    {
        memset(static_cast<void *>(pngfile), 0, sizeof(pngfile));
        memset(static_cast<void *>(method), 0, sizeof(method));

    }

const GPESolver::Complex GPESolver::ii = GPESolver::Complex(0.0, 1.0);
const GPESolver::Float GPESolver::pi = std::acos(-1);

GPESolver::GPESolver(const GPEConfig& cfg)
    : cfg_(cfg),
      curr_interval_(0)
    {
        if(cfg_.left_bound < 0 || cfg_.right_bound < 0)
        {
            throw std::out_of_range("Invalid boundaries values");
        }
        coords_.resize(cfg_.xpoints);
        coords_.reserve(cfg_.xpoints);
        for(size_t i = 0; i < coords_.size(); ++i)
        {
            coords_[i] = i * cfg_.xstep;
        }
    }

void GPESolver::ComputeInitialValues()
{
    initial_.resize(cfg_.xpoints);
    initial_.reserve(cfg_.xpoints);
    initial_[0] = cfg_.left_bound;
    for(size_t i = 1; i < cfg_.xpoints - 1; ++i)
    {
        // Float x = cfg_.xstep * i;
        // initial_[i] = cfg_.ampl * std::sin(x*pi);
        initial_[i] = 0.5;
    }
    initial_[cfg_.xpoints - 1] = cfg_.right_bound;
    current_ = initial_;
}

} // namespace mephi
